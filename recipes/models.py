from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    # automatically set when created: created time
    updated = models.DateTimeField(auto_now=True)
    # automatically set when updated: updated time

    def __str__(self):
        return self.name.title()


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
     # we dont need a same food item added to the
    # list for many times, so the name should be
    #unique
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.abbreviation

class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)
    # we dont need a same food item added to the
    # list for many times, so the name should be
    #unique

    def __str__(self):
        return self.name.lower()

class Ingredient(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE
    )
    measure = models.ForeignKey(
        "Measure",
        on_delete=models.PROTECT
    )
    food = models.ForeignKey(
        "FoodItem",
        on_delete=models.PROTECT
    )
    # 如果related name填的不对，网页上会显示不出input
    def __str__(self):
        return f"{str(self.amount)}{self.measure} {self.food}"


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete=models.CASCADE
    )
    order = models.SmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField(
        "FoodItem",
        related_name="steps",
        blank=True
    )

    def __str__(self):
        return self.directions
